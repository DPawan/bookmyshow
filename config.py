import os


class Config(object):
    SQLALCHEMY_DATABASE_URI = "postgresql:///appdb"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'my-simple-secret-key'
    ELASTICSEARCH_URL = 'http://localhost:9200'
    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'