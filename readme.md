1. install prostgresql
    - sudo apt update 
    - sudo apt install postgresql
2. create a roll for your machine
    - sudo su - postgres
    - psql -U postgres
    - create role ubuntu with login superuser;
3. create database with the same name you used in application
    - createdb appdb
4. clone the application
    - git clone https://gitlab.com/DPawan/bookmyshow.git
    - sudo rm -rf venv
    - sudo rm -rf migrations
5. install redis
    - sudo apt install redis
6. make a virtualenv and install all the dependecies from requirement.txt
    - sudo apt install python3-venv
    - python3 -m venv venv
    - source venv/bin/activate
    - sudo apt install libpq-dev python3-dev
    - pip3 install -r requirements.txt
7. run flask migrations and upgrade it
    - flask db init
    - flask db migrate
    - flask db upgrade
8. make systemd service for app in /etc/systemd/system
    `[Unit]

    Description=Gunicorn instance to serve myproject
    After=network.target

    [Service]
    User=ubuntu
    Group=www-data
    WorkingDirectory=/home/ubuntu/bookmyshow
    Environment="PATH=/home/ubuntu/bookmyshow/venv/bin"

    ExecStart=/home/ubuntu/bookmyshow/venv/bin/gunicorn --workers 3 --bind unix:bookmyshow.sock -m 007 wsgi:app --access-logfile access.log --error-logfile error.log

    [Install]
    WantedBy=multi-user.target`
9. reload the daemon
    sudo systemctl daemon-reload
    sudo systemctl start app
10. install nginx and setup app with nginx in /etc/nginx/sites-enabled
    - sudo apt install nginx
    - move to /etc/nginx/sites-enabled
    - `
        server{
            listen 80;
            server_name 15.206.94.28;

            location / {
            include proxy_params;
            proxy_pass http://unix:/home/ubuntu/bookmyshow/bookmyshow.sock;
            }
        }`
    - sudo systemctl restart nginx
    - sudo ufw allow 'Nginx Full'

11. make celery environment at /etc/default
    - sudo nano celeryd
    - `#workers
        CELERYD_NODES="worker"

        #Where your Celery is present
        CELERY_BIN="/home/ubuntu/bookmyshow/venv/bin/celery"

        # App instance to use 
        CELERY_APP="app.celery"
        CELERYD_OPTS="--time-limit=300 --concurrency=8"
        CELERYD_PID_FILE="/var/run/celery/%n.pid"
        CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
        CELERYD_LOG_LEVEL="INFO"
        # You need to create this user manually (or you can choose
        # A user/group combination that already exists (e.g., nobody). 
        CELERYD_USER="ubuntu" 
        CELERYD_GROUP="ubuntu"

        # Log and PID directories
        CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
        CELERYD_PID_FILE="/var/run/celery/%n.pid"
        #for celery beat
        CELERYBEAT_PID_FILE="/var/run/celery/beat.pid"
        CELERYBEAT_LOG_FILE="/var/log/celery/beat.log"`
12. make a celery service in systemd
    - move to /etc/systemd/system
    - sudo nano celeryd.service
    - `[Unit]
        Description=Celery Service
        After=network.target

        [Service]
        Type=forking
        User=ubuntu
        Group=ubuntu
        EnvironmentFile=/etc/default/celeryd
        WorkingDirectory=/home/ubuntu/bookmyshow
        ExecStart=/home/ubuntu/bookmyshow/venv/bin/celery multi start single-worker -A app.celery --pidfile=/var/run/celery/single.pid --logfile=/var/log/celery/single.log "-c 4 -Q celery -l INFO"
        ExecStop=/home/ubuntu/bookmyshow/venv/bin/celery multi stopwait single-worker --pidfile=/var/run/celery/single.pid --logfile=/var/log/celery/single.log
        ExecReload=/home/ubuntu/bookmyshow/venv/bin/celery multi restart single-worker --pidfile=/var/run/celery/single.pid --logfile=/var/log/celery/single.log
        [Install]
        WantedBy=multi-user.target`
13. make a celery beat service in systemd
    - sudo nano celerybeat.service
    - `[Unit]
        Description=Celery Service
        After=network.target

        [Service]
        Type=simple
        User=ubuntu
        Group=ubuntu
        EnvironmentFile=/etc/default/celeryd
        WorkingDirectory=/home/ubuntu/bookmyshow
        ExecStart=/bin/sh -c '${CELERY_BIN} beat  \
        -A ${CELERY_APP} --pidfile=${CELERYBEAT_PID_FILE} \
        --logfile=${CELERYBEAT_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL}'

        [Install]
        WantedBy=multi-user.target`
14. make log and run paths for celery
    - sudo mkdir /var/log/celery /var/run/celery
    - sudo chown ubuntu:ubuntu /var/log/celery /var/run/celery

15. reload daemon and start the celery worker
    - sudo systemctl daemon-reload
    - sudo systemctl enable celeryd
    - sudo systemctl enable celerybeat
    - sudo systemctl start celeryd
    - sudo systemctl start celerybeat