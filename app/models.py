from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db
from app import login
from datetime import datetime


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    isorganiser = db.Column(db.Boolean(), unique=False, default=False)
    email = db.Column(db.String(120), unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    eventname = db.Column(db.String(64), unique=True)
    poster = db.Column(db.String(120), default='image.jpg', nullable=False)
    time = db.Column(db.Time, nullable=False)
    date = db.Column(db.Date, nullable=False)
    availableseats = db.Column(db.Integer, nullable=False)
    venue = db.Column(db.String(120), nullable=False)
    organiserid = db.Column(db.Integer, db.ForeignKey('user.id'))
    aboutevent = db.Column(db.String(200))
    price = db.Column(db.Integer, nullable=False)
    __searchable__ = ['eventname', 'aboutevent']

    def __repr__(self):
        return '<Event {}>'.format(self.eventname)


class Booking(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    eventid = db.Column(db.Integer, db.ForeignKey('event.id'))
    fanid = db.Column(db.Integer, db.ForeignKey('user.id'))
    numberoftickets = db.Column(db.Integer)
    date = db.Column(db.Date, default=datetime.now().date())
    time = db.Column(db.Time, default=datetime.now().time())
    totalcost = db.Column(db.Integer)

class Type(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    eventid = db.Column(db.Integer, db.ForeignKey('event.id'), nullable=False)
    type = db.Column(db.String(120), nullable=False)