from flask import flash
from app import db
from app.models import User


def register_user(form):
    user = User(username=form.username.data, email=form.email.data,
     isorganiser=form.isorganiser.data)
    user.set_password(form.password.data)
    db.session.add(user)
    db.session.commit()
    flash('Congratulations, you are now a registered user!')
    return user
