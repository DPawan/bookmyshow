from flask import render_template, flash, redirect, url_for, session
from app.user.forms import LoginForm, RegistrationForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Booking, Event
from app.user import bp
from app.user.services import register_user
from app.event.forms import SearchForm


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('event.home'))
    form = LoginForm()
    print(session)
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        print(session)
        return redirect(url_for('event.home'))
    return render_template('user/login.html', title='Login', form=form)


@bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('user.login'))


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('event.home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = register_user(form)
        login_user(user)
        return redirect(url_for('event.home'))
    return render_template('user/register.html', title='Register', form=form)


@bp.route('/user/<username>')
@login_required
def user_profile(username):
    search_form = SearchForm()
    if not current_user.isorganiser:
        bookings = Booking.query.filter_by(fanid=current_user.id)
        return render_template('user/user.html', bookings=bookings, event=Event, search_form=search_form)
    else:
        events = Event.query.filter_by(organiserid=current_user.id)
        return render_template('user/user.html', events=events, search_form=search_form)
