from app import app
from flask import render_template, request, url_for

@app.errorhandler(404)
def not_found(e):
    return render_template("error.html", message="Page not found")


@app.errorhandler(500)
def not_found(e):
    return render_template("error.html", message="server error")


@app.errorhandler(403)
def not_found(e):
    return render_template("error.html", message="Permision denied")

