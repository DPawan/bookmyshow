from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, FileField, TextAreaField, SelectMultipleField
from wtforms.validators import DataRequired, NumberRange
from wtforms_components import TimeField, DateField
from flask import request


class AddEventForm(FlaskForm):
    eventname = StringField('Event Name', validators=[DataRequired()])
    poster = FileField('Poster', validators=[DataRequired()])
    eventtype = SelectMultipleField(u'Event Type', choices=[('Movie', 'Movie'),
     ('Concert', 'Concert'), ('Sport', 'Sport')])
    venue = StringField('Venue', validators=[DataRequired()])
    time = TimeField('Time of event', validators=[DataRequired()])
    date = DateField('Date of event', validators=[DataRequired()])
    availableseats = IntegerField('Available seats', validators=[DataRequired()])
    price = IntegerField('Price per ticket', validators=[DataRequired()])
    details = TextAreaField('Description of event')
    submit = SubmitField('Add')


class UpdateEventForm(FlaskForm):
    poster = FileField('Poster', validators=[DataRequired()])
    venue = StringField('Venue', validators=[DataRequired()])
    time = TimeField('Time of event', validators=[DataRequired()])
    date = DateField('Date of event', validators=[DataRequired()])
    availableseats = IntegerField('Available seats', validators=[DataRequired()])
    price = IntegerField('Price per ticket', validators=[DataRequired()])
    details = StringField('Description of event')
    submit = SubmitField('Update')


class BookTicket(FlaskForm):
    numberoftickets = IntegerField('Number of tickets you want', validators=[
        NumberRange(min=1, max=5)])
    submit = SubmitField('Book')


class SearchForm(FlaskForm):
    q = StringField('Search', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)


class DownloadForm(FlaskForm):
    submit = SubmitField('Download')
