import os
import hashlib
from flask import flash, current_app, request, make_response
from flask_login import current_user
from app import db, celery
import random
from app.models import Event, Booking, Type
from app.search import add_to_index


def save_image(photo):
    hash_photo = hashlib.md5(str(random.randint(1, 100000000000)).encode('utf-8')).hexdigest()
    _, file_extention = os.path.splitext(photo.filename)
    photo_name = hash_photo + file_extention
    file_path = os.path.join(current_app.root_path, 'static/img', photo_name)
    photo.save(file_path)
    return photo_name


def book_tickets(numberoftickets, event):
    event.availableseats = event.availableseats - numberoftickets
    totalcost = numberoftickets*(event.price)
    booking = Booking(totalcost=totalcost, eventid=event.id, fanid=current_user.id, numberoftickets=numberoftickets)
    db.session.add(booking)
    db.session.commit()
    event = Event.query.get(booking.eventid)
    flash('Kudos, booking successfull')
    return booking.id


def add_event(form):
    poster = save_image(request.files.get('poster'))
    event = Event(price=form.price.data, eventname=form.eventname.data,
     poster=poster, time=form.time.data, date=form.date.data,
      availableseats=form.availableseats.data,
       venue=form.venue.data, organiserid=current_user.id, aboutevent=form.details.data)
    # print(form.poster.data)
    db.session.add(event)
    db.session.commit()
    add_to_index('events', event)
    types = form.eventtype.data
    for type in types:
        thistype = Type(eventid=event.id, type=type)
        db.session.add(thistype)
        db.session.commit()
        add_to_index(type, event)
    flash('Event added succesfully')
    return event.id


def update_event(event, form, eventid):
    poster = save_image(request.files.get('poster'))
    event.venue = form.venue.data
    event.time = form.time.data
    event.date = form.date.data
    event.availableseats = form.availableseats.data
    event.aboutevent = form.details.data
    event.poster = poster
    event.price = form.price.data
    db.session.commit()
    add_to_index('events', event)
    flash('Event Updated succesfully')


@celery.task(bind=True)
def async_download_ticket(self, bookingid, eventid):
    booking = Booking.query.get(bookingid)
    event = Event.query.get(eventid)
    f = open('app/static/tickets/{}.txt'.format(bookingid), 'w+')
    f.write(str(booking.__dict__))
    f.write(str(event.__dict__))
    f.close()
    return '{}.txt'.format(bookingid)


def download_it(path):
    f = open(path, 'r')
    response = make_response(f.read())
    f.close()
    response.headers['Content-Type'] = 'text/html'
    response.headers['Content-Disposition'] = 'attachment; filename=bookmyshow.txt'
    return response
