from flask import render_template, flash, redirect, url_for, g, jsonify, request, current_app, abort
from app.event import bp
from flask_login import login_required, current_user
from app.event.forms import AddEventForm, UpdateEventForm, BookTicket, SearchForm
from app.models import Event, Booking, Type
from app.event.services import book_tickets, add_event, update_event, async_download_ticket, download_it
from app.search import query_index


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        g.search_form = SearchForm()


@bp.route('/', methods=['GET'])
@bp.route('/home', methods=['GET'])
@login_required
def home():
    events = Event.query.all()
    search_form = SearchForm()
    return render_template('event/home.html', title='Home', events=events, search_form=search_form)


@bp.route('/booking/<bookingid>', methods=['GET'])
@login_required
def booking(bookingid):
    booking = Booking.query.get(bookingid)
    if not booking:
        abort(404)
    if (booking.fanid != current_user.id):
        abort(403)
    event = Event.query.get(booking.eventid)
    search_form = SearchForm()
    return render_template('event/booking.html', title='success', booking=booking, event=event, type=Type, search_form=search_form)



@bp.route('/event/<eventid>', methods=['GET', 'POST'])
@login_required
def event_detail(eventid):
    event = Event.query.get(eventid)
    if not event:
        abort(404)
    if not current_user.isorganiser:
        form = BookTicket()
        if form.validate_on_submit():
            if form.numberoftickets.data > event.availableseats:
                flash('Not available')
                return redirect(url_for('event.home'))
            bookingid = book_tickets(form.numberoftickets.data, event)
            return redirect(url_for('event.booking', bookingid=bookingid))
        search_form = SearchForm()
        return render_template('event/event.html', event=event, form=form, type=Type, search_form=search_form)
    else:
        search_form = SearchForm()
        return render_template('event/event.html', event=event, type=Type, search_form=search_form)


@bp.route('/event/add', methods=['GET', 'POST'])
@login_required
def addevent():
    if not current_user.isorganiser:
        abort(403)
    form = AddEventForm()
    if form.validate_on_submit():
        eventName = form.eventname.data
        event = Event.query.filter_by(eventname=eventName).first()
        if event:
            flash('That event has already been added (Try changing the event name)')
            return redirect(url_for('event.home'))
        eventid = add_event(form)
        return redirect(url_for('event.event_detail', eventid=eventid))
    return render_template('event/addevent.html', title='Add event', form=form)


@bp.route('/event/update/<eventid>', methods=['GET', 'POST'])
@login_required
def updateevent(eventid):
    event = Event.query.get(eventid)
    if not event:
        abort(404)
    if not current_user.isorganiser:
        abort(403)
    form = UpdateEventForm(poster=event.poster, venue=event.venue,
        time=event.time, date=event.date, availableseats=event.availableseats,
        price=event.price, details=event.aboutevent)
    if form.validate_on_submit():
        if event.organiserid != current_user.id:
            abort(403)
        update_event(event, form, form)
        return redirect(url_for('event.event_detail', eventid=eventid))
    return render_template('event/updateevent.html', title='Add event', form=form, event=event)


@bp.route('/event/type/<type>', methods=['GET'])
@login_required
def event_type(type):
    events = Type.query.filter_by(type=type)
    search_form = SearchForm()
    return render_template('event/type.html', title=type, events=events, Event=Event, search_form=search_form)


@bp.route('/search', methods=['GET'])
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('event.home'))
    if not current_app.elasticsearch.ping():
        flash('Search is not available')
        return redirect(url_for('event.home'))
    eventids, total = query_index('events', g.search_form.q.data)
    events = []
    for eventid in eventids:
        events.append(Event.query.get(eventid))
    search_form = SearchForm()
    return render_template('event/search.html', title='search', events=events, search_form=search_form)


@bp.route('/event/<bookingid>/download_ticket', methods=['GET', 'POST'])
@login_required
def download_ticket(bookingid):
    booking = Booking.query.get(bookingid)
    if not booking:
        abort(404)
    if(booking.fanid != current_user.id):
        abort(403)
    event = Event.query.get(booking.eventid)
    if request.method == 'POST':
        task = async_download_ticket.apply_async(args=[bookingid, event.id])
        return jsonify({}), 202, {'Location': url_for('event.status', taskid=task.id), 'id': task.id}
    return render_template('event/ticket.html', booking=booking, event=event, type=Type)


@bp.route('/status/<taskid>', methods=['GET'])
def status(taskid):
    task = async_download_ticket.AsyncResult(taskid)
    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    return jsonify(response)


@bp.route('/download/<taskid>', methods=['GET'])
def download(taskid):
    task = async_download_ticket.AsyncResult(taskid)
    return download_it('./app/static/tickets/{}'.format(task.get()))
