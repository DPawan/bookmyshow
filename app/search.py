from flask import current_app, flash


def add_to_index(index, model):
    if not current_app.elasticsearch.ping():
        return
    payload = {}
    for field in model.__searchable__:
        payload[field] = getattr(model, field)
    current_app.elasticsearch.index(index=index, id=model.id, body=payload)


def query_index(index, query):
    if not current_app.elasticsearch.ping():
        flash('Search is not available')
        return
    search = current_app.elasticsearch.search(
        index=index,
        body={
            'query': {'multi_match': {'query': query, 'fields': ['*']}}})
    ids = [int(hit['_id']) for hit in search['hits']['hits']]
    return ids, search['hits']['total']['value']
