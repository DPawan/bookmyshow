from app import app, db
from app.models import User, Event


@app.shell_context_processor
def shell_context_processor():
    return {'db': db, 'User': User, 'Event': Event}
